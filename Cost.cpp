// Cost.cpp file for the cost.h

#include "Cost.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <iterator>

    // constructor
    Cost::Cost(std::vector<double> X, int Fmin, int Fmax, int Fs, double Resolution, char* CostOutputFileName)
    {
        resolution=Resolution;
        VectorSize = round((Fmax-Fmin)/Resolution+1);
        fmin=Fmin;
        fmax=Fmax;
        fs=Fs;
        costoutputFileName  = CostOutputFileName;
        
        NFFT = 2*Fs;
        
        f0Area.resize(VectorSize);
        x.resize(VectorSize);
        x=X;
        costfunction.resize(VectorSize);

        f0Area=smc_F0Area(fmin, fmax, resolution);
        costfunction=smc_CreateCost(x,fmin, fmax);
    }

    std::vector<double> Cost::smc_F0Area(int Fmin, int Fmax, double Resolution)
    {

        std::vector<double> F0Area (VectorSize);
    
        F0Area[0]=Fmin;
        for(std::vector<int>::size_type i=1; i!=F0Area.size(); i++)
        {
            F0Area[i]=F0Area[i-1]+Resolution;
        }
        return F0Area;

    }

    std::vector <double> Cost::smc_CreateCost(std::vector<double> X, int Fmin, int Fmax)
    {
        FILE *cost_w;
        std::vector<double>F0Area (VectorSize);  
        F0Area = f0Area;
        std::vector<double> Costs (VectorSize);
        int index;
        int L=5;

        /* Open file pointer writable */
        cost_w = fopen(costoutputFileName, "w");
    
        for(std::vector<int>::size_type k=0; k!=F0Area.size(); k++)
        {
        index = round(F0Area.at(k)*(2*NFFT/fs)+1);      
        Costs.at(k) = X.at(index);
       
        for (int l = 2; l < L+1; ++l)
        {
            index = round(F0Area[k]*l*(2*NFFT/fs));
            Costs.at(k) += X.at(index);
        }
            fprintf(cost_w, "%f \t %f\n", F0Area[k], Costs[k]);  
        }
        return Costs;
    }

    double Cost::smc_MaxCost()
    {
        int maxIdx;
        double maxVal;
    
        maxVal=costfunction[0];
          
        for(int j = 0; j != costfunction.size(); j++)
        {
            if (maxVal < costfunction[j])
            {
                maxVal=costfunction[j];
                maxIdx=j;
            }
        }
    double pitch = f0Area.at(maxIdx);
    return pitch; 
    }

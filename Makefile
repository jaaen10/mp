OBJS = main.cpp FrequencyDomain.cpp Cost.cpp TimeDomain.cpp
CC = g++
CFLAGS = -lfftw3 -lsndfile

p1: $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) -o p1

run: p1
	./p1

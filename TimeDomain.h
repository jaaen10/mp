//
//  TimeDomain.h
//  
//
//  Created by Jakob Aaen on 11/03/15.
//
//

#ifndef ____TimeDomain__
#define ____TimeDomain__
#include "fftw3.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <iterator>
#include <stdio.h>

class TimeDomain
{
    char* inputFileName;
    char* outputFileName;
    float* signal;
    
    float* smc_wavread();
     
    public:
        float* GetSignal();
        TimeDomain(char InputFileName[], char OutputFileName[]);
    
};

#endif /* defined(____TimeDomain__) */

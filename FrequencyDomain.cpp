#include "FrequencyDomain.h"
#include <stdio.h>
#include "fftw3.h"
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <iostream>

std::vector <double> FrequencyDomain::GetFFT()
{
    return fFT;
}

void FrequencyDomain::SetInputSignal(float *InputSignal, int Fs, char *FFTOutputFileName) //last part new
{
    fFTOutputFileName = FFTOutputFileName;
    inputSignal = InputSignal;
    fs = Fs;
    nFFT=2*Fs;
    
}

void FrequencyDomain::CalculateFFT(void)
{
    FILE *fp_open_r;
    FILE *fp_open_w;
    
    fftw_complex *in, *out;
    fftw_plan p;
    
    in  = ( fftw_complex* ) fftw_malloc(sizeof(fftw_complex) * nFFT);
    out = ( fftw_complex* ) fftw_malloc(sizeof(fftw_complex) * nFFT);
    p   = fftw_plan_dft_1d(nFFT, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    int i;
    for (i = 0; i < nFFT; i++)
    {
        in[i][0] = inputSignal[i];
        in[i][1] = 0.0;
    }
    
    fftw_execute(p);   
    /* Open file pointer writable */
    fp_open_w = fopen(fFTOutputFileName, "w");

    fftw_free(in);  
    std::vector <double> X (round(nFFT/2));
    std::vector <double> FFTAxis (round(nFFT/2));
  
    for (i = 0; i < round(nFFT/2); i++)
    {
        X.at(i) = (double)(sqrt((out[i][0])*(out[i][0]) + (out[i][1])*(out[i][1])));
        FFTAxis.at(i) = ((double)i)*fs/nFFT;
        fprintf(fp_open_w, "%f \t %f\n", FFTAxis[i], X[i]);  
    }
    
    fFT.resize(X.size());
    fFT =  X;

    fftw_destroy_plan(p);  
    fftw_free(out);      
}



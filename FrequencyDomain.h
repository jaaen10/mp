#ifndef ____FrequencyDomain__
#define ____FrequencyDomain__

#include <stdio.h>
#include <vector>

class FrequencyDomain
{
    std::vector<double> fFT;
    float *inputSignal;
    char *outputFileName;
    int fs;
    int nFFT;
    char *fFTOutputFileName;

public:
    std::vector<double> GetFFT();
    void SetInputSignal(float *InputSignal, int Fs, char*outputFileName);
    void CalculateFFT(void);
    
};

#endif /* defined(____FrequencyDomain__) */

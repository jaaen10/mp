#include "fftw3.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <iterator>
#include "Cost.h"
#include "FrequencyDomain.h"
#include "TimeDomain.h"


int main()
{
    int option = 1;
    do
    {
            std::cout<< "Please Specify Input File For Pitch Estimation by type one of: \n ./data/E_87.wav:\n ./data/A_110.wav\n ./data/D_147.wav\n";
            char in[100];
            std::cin >> in;
            char out[] = "data/input_signal.txt";
            char outTest[] = "data/X.txt";
            char outCost[] = "data/cost.txt";
            
            //Declerations
            double Resolution=0.1;    
            double pitch;   
            std::vector<double> Z;    
            int Fs=44100, FMax=700, FMin=75;  
            float* sig;
            
            //Using Functions
            TimeDomain Somesig(in, out);
            sig = Somesig.GetSignal();
                   
            FrequencyDomain F1;       
            F1.SetInputSignal(sig, Fs, outTest);        
            F1.CalculateFFT();      
            Z = F1.GetFFT();
                 
            Cost test(Z,FMin,FMax,Fs,Resolution, outCost);       
            pitch = test.smc_MaxCost();
            
            std::cout << "pitch is: " << pitch << "\n\n";

    }
    while(option != 2);
    return 0;
}
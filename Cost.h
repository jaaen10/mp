// this is a .h file to Cost

#ifndef ____Cost__
#define ____Cost__

#include <stdio.h>
#include <vector>

class Cost
{
private:
	int NFFT;
	int L;
	int fmin, fmax, fs;
	double resolution;
	int VectorSize;
	char* costoutputFileName;

	std::vector<double> x;
	std::vector<double> f0Area;
	std::vector<double> costfunction;

	std::vector<double> smc_F0Area(int Fmin, int Fmax, double Resolution);
	std::vector<double> smc_CreateCost(std::vector<double> X,int Fmin, int Fmax);

public:

	double smc_MaxCost();
	// constructor
	Cost(std::vector<double> X, int Fmin, int Fmax, int fs, double resolution, char* CostOutputFileName);

};
#endif 
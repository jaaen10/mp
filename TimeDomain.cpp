//
//  TimeDomain.cpp
//  
//
//  Created by Jakob Aaen on 11/03/15.
//
//
#include "fftw3.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sndfile.h>
#include <fstream>
#include <vector>
#include <cmath>
#include <iterator>
#include "TimeDomain.h"

TimeDomain::TimeDomain(char InputFileName[], char OutputFileName[])
{
    inputFileName = InputFileName;
    outputFileName  = OutputFileName;
    
    signal = smc_wavread();
}

float* TimeDomain::GetSignal()
{
    return signal;
}

float* TimeDomain::smc_wavread()
{
    SNDFILE *sf;
    SF_INFO info;
    int num_channels;
    int numSamples, samplesTotal;
    float *inputBuffer;
    int N, Fs, numChannels;
    int i,j;
    FILE *out;
    
    /* Open the WAV file. */
    info.format = 0;
    sf = sf_open(inputFileName, SFM_READ, &info);
    if (sf == NULL)
    {
        printf("Failed to open the file.\n");
        exit(-1);
    }
    /* Print some of the info, and figure out how much data to read. */
    N = info.frames; Fs = info.samplerate; numChannels = info.channels;
    //printf("\nN=%d\nsamplerate=%d\nchannels=%d\n", N, Fs, numChannels);
    numSamples = N*numChannels;
    //printf("numSamples=%d\n",numSamples);
    
    /* Allocate space for the data to be read, then read it. */
    inputBuffer = (float *) malloc(numSamples*sizeof(float));
    samplesTotal = sf_read_float(sf, inputBuffer, numSamples);
    sf_close(sf);
    
    printf("Read %d items in total\n\n",samplesTotal);
    /* Write the data to filedata.out. */
    out = fopen(outputFileName,"w");
    
    for (i = 0; i < samplesTotal; i += numChannels)
    {
        for (j = 0; j < numChannels; ++j)
            fprintf(out,"%f ",inputBuffer[i+j]);
        fprintf(out,"\n");
    }
    return inputBuffer;
    free(inputBuffer);
    fclose(out);
}